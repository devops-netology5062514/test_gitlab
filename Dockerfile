FROM centos:7
RUN yum  install python3 python3-pip -y
COPY requirement.txt requirement.txt
RUN pip3 install -r requirement.txt
WORKDIR /python_api
COPY app.py python-api.py
CMD ["python3","python-apy.py"]
